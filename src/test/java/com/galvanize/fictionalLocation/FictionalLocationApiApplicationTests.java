package com.galvanize.fictionalLocation;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FictionalLocationApiApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;
	private RestTemplate patchRestTemplate;

	@Autowired
	LocationRepository locationRepository;

	List<Location> testLocations;

	@BeforeEach
	void setUp() {
		this.patchRestTemplate = restTemplate.getRestTemplate();
		HttpClient httpClient = HttpClientBuilder.create().build();
		this.patchRestTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

		this.testLocations = new ArrayList<>();
		Location location;

		for (int i = 0; i < 50; i++) {
			if (i % 3 == 0) {
				location = new Location("1" + (i*13) + " Main St", "Bloomington", "IL", "61761");
			} else if (i % 2 == 0) {
				location = new Location("2" + (i*12) + " East St", "Bloomington", "IL", "61761");
			} else {
				location = new Location("3" + (i*11) + " South St", "Bloomington", "IL", "61761");
			}
			this.testLocations.add(location);
		}
		locationRepository.saveAll(this.testLocations);
	}

	@AfterEach
	void tearDown() {
		locationRepository.deleteAll();
	}

	@Test
	void getLocations_exists_returnsLocationsList() {
		ResponseEntity<Location[]> response = restTemplate.getForEntity("/api/fictionalLocation", Location[].class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).isNotNull();

		for (int i = 0; i < response.getBody().length; i++) {
			System.out.println(response.getBody()[i].toString());
		}
	}

	@Test
	void getLocation_search_returnsLocation() {
		ResponseEntity<Location> response = restTemplate.getForEntity(
				"/api/fictionalLocation/10 Main St/Bloomington", Location.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		System.out.println(response.getBody().toString());
	}

	@Test
	void addLocation_returnsNewLocationDetails() {
		Location location = new Location("2020 Mushroom Castle", "Magic Kingdom", "ML", "99999");

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<Location> request = new HttpEntity<>(location, headers);

		ResponseEntity<Location> response = restTemplate.postForEntity("/api/fictionalLocation", request, Location.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody().getStAddress()).isEqualTo(location.getStAddress());
	}

	@Test
	public void getLocationAndPatch() throws JSONException {
		String resourceUrl = "/api/fictionalLocation/10 Main St/Bloomington";

		JSONObject updateBody = new JSONObject();
		updateBody.put("charName", "Wario");

		ResponseEntity<Location> responseEntity =
				patchRestTemplate.exchange(resourceUrl, HttpMethod.PATCH,
						getPostRequestHeaders(updateBody.toString()), Location.class);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals("Wario", responseEntity.getBody().getCharName());
	}

	@Test
	void deleteLocation_thenReturnStatus204() {
		String resourceUrl = "/api/fictionalLocation/10 Main St/Bloomington";

		patchRestTemplate.delete(resourceUrl);
		ResponseEntity<Location> responseEntity = patchRestTemplate.getForEntity(resourceUrl, Location.class);

		assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
	}

	public HttpEntity getPostRequestHeaders(String jsonPostBody) {
		List acceptTypes = new ArrayList();
		acceptTypes.add(MediaType.APPLICATION_JSON);

		HttpHeaders reqHeaders = new HttpHeaders();
		reqHeaders.setContentType(MediaType.APPLICATION_JSON);
		reqHeaders.setAccept(acceptTypes);

		return new HttpEntity(jsonPostBody, reqHeaders);
	}
}
