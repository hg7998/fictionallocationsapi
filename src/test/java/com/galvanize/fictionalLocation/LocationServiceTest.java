package com.galvanize.fictionalLocation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)

public class LocationServiceTest {
    private LocationService locationService;

    @Mock
    LocationRepository locationRepository;

    @BeforeEach
    void setUp() { locationService = new LocationService(locationRepository);
    }

    @Test
    void getLocations_noArgs_returnsList() {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");
        Location location2 = new Location("150 N Main St", "Bloomington", "IL", "61761");
        List<Location> locations = new ArrayList<>();

        locations.add(location);
        locations.add(location2);

        when(locationRepository.findAll()).thenReturn(locations);
        locations = locationService.getAllLocations();

        assertThat(locations).isNotNull();
        assertThat(locations.isEmpty()).isFalse();
    }

    @Test
    void getLocation_searchByCharName_returnsLocation() {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");
        location.setCharName("Ganondorf");

        when(locationRepository.findByCharName(anyString()))
                .thenReturn(Optional.of(location));
        Location outputLocation = locationService.getLocation("Ganondorf");

        assertThat(outputLocation).isNotNull();
        assertThat(outputLocation.getCharName()).isEqualTo(location.getCharName());
    }

    @Test
    void getLocation_searchByStreetAndCity_returnsLocation() {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");

        when(locationRepository.findByStAddressAndCity(anyString(), anyString()))
                .thenReturn(Optional.of(location));
        Location outputLocation = locationService.getLocation("130 N Main St", "Bloomington");

        assertThat(outputLocation).isNotNull();
        assertThat(outputLocation.getStAddress()).isEqualTo(location.getStAddress());
        assertThat(outputLocation.getCity()).isEqualTo(location.getCity());
    }

    @Test
    void updateLocation_addChar_returnsLocationWithChar() {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");

        when(locationRepository.findByStAddressAndCity(anyString(), anyString()))
                .thenReturn(Optional.of(location));
        when(locationRepository.save(any(Location.class))).thenReturn(location);
        Location outputLocation = locationService.updateLocationAddChar("130 N Main St", "Bloomington", "Daisy");

        assertThat(outputLocation).isNotNull();
        assertThat(outputLocation.getStAddress()).isEqualTo(location.getStAddress());
        assertThat(outputLocation.getCity()).isEqualTo(location.getCity());
        assertThat(outputLocation.getCharName()).isEqualTo("Daisy");
    }

    @Test
    void updateLoc_addChar_movesCharFromOtherLoc_returnsLocWithCharAndLocWithoutChar() {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");
        Location location2 = new Location("150 N Main St", "Bloomington", "IL", "61701");

        when(locationRepository.findByStAddressAndCity(anyString(), anyString()))
                .thenReturn(Optional.of(location));
        when(locationRepository.save(any(Location.class))).thenReturn(location);
        location = locationService.updateLocationAddChar("130 N Main St", "Bloomington", "Daisy");

        when(locationRepository.findByStAddressAndCity(anyString(), anyString()))
                .thenReturn(Optional.of(location2));
        when(locationRepository.findByCharName(anyString())).thenReturn(Optional.of(location));
        when(locationRepository.save(any(Location.class))).thenReturn(location2);
        location2 = locationService.updateLocationAddChar("150 N Main St", "Bloomington", "Daisy");

        assertThat(location).isNotNull();
        assertThat(location2).isNotNull();
        assertThat(location.getCharName()).isNull();
        assertThat(location2.getCharName()).isEqualTo("Daisy");
    }

    @Test
    void getLocationByStAddressAndCity_NoLocationExists_ReturnsNull() throws InvalidLocationException {
        when(locationRepository.findByStAddressAndCity(anyString(), anyString()))
                .thenReturn(Optional.empty());
        Location location = locationService.getLocation(anyString(), anyString());
        assertNull(location);
    }

    @Test
    void getLocationByCharName_ReturnsNull() throws InvalidLocationException {
        when(locationRepository.findByCharName(anyString()))
                .thenReturn(Optional.empty());
        Location location = locationService.getLocation(anyString());
        assertNull(location);
    }

    @Test
    void addLocation_returnsAddedLocation() {
        Location location = new Location("200 N Main St", "Bloomington", "IL", "61701");

        when(locationRepository.findByStAddressAndCity(anyString(),anyString())).thenReturn(Optional.empty());
        when(locationRepository.save(location)).thenReturn(location);
        Location response = locationService.addLocation(location);

        assertThat(response).isNotNull();
        assertThat(response.getStAddress()).isEqualTo("200 N Main St");
    }

    @Test
    void deleteLocation_returnsDeletedLocation() {
        Location location = new Location("200 N Main St", "Bloomington", "IL", "61701");

        when(locationRepository.findByStAddressAndCity(anyString(),anyString())).thenReturn(Optional.empty());
        when(locationRepository.save(location)).thenReturn(location);
        Location response = locationService.addLocation(location);

        assertThat(response).isNotNull();

        when(locationRepository.findByStAddressAndCity(anyString(),anyString())).thenReturn(Optional.of(location));
        locationService.deleteLocation(response.getStAddress(), response.getCity());

        verify(locationRepository).delete(any(Location.class));
    }

    @Test
    void deleteLocation_AddressDoesntExist_returnsInvalidLocationException() {
        when(locationRepository.findByStAddressAndCity(anyString(), anyString()))
                .thenReturn(Optional.empty());
        assertThatExceptionOfType(InvalidLocationException.class)
                .isThrownBy(() -> {
                    locationService.deleteLocation(anyString(),anyString());
                });
    }

    @Test
    void removeChar_returnsLocationWithoutCharacter() {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");

        when(locationRepository.findByStAddressAndCity(anyString(), anyString()))
                .thenReturn(Optional.of(location));
        when(locationRepository.save(any(Location.class))).thenReturn(location);
        locationService.updateLocationAddChar("130 N Main St", "Bloomington", "Daisy");

        assertThat(location.getCharName()).isEqualTo("Daisy");

        locationService.updateLocationRemoveChar(anyString(),anyString());

        assertThat(location.getCharName()).isNull();
        assertThat(location.getStAddress()).isNotNull();
    }

    @Test
    void removeChar_locationDoesntExist_returnsInvalidLocationException() {
        when(locationRepository.findByStAddressAndCity(anyString(), anyString()))
                .thenReturn(Optional.empty());

        assertThatExceptionOfType(InvalidLocationException.class)
                .isThrownBy(() -> {
                    locationService.updateLocationRemoveChar(anyString(),anyString());
                });
    }

    @Test
    void removeChar_locationDoesntHaveChar_returnsCharAsNullAndDoesntBreak() {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");

        when(locationRepository.findByStAddressAndCity(anyString(),anyString())).
                thenReturn(Optional.of(location));
        when(locationRepository.save(any(Location.class))).thenReturn(location);
        locationService.updateLocationRemoveChar(anyString(),anyString());

        assertThat(location.getCharName()).isNull();
        assertThat(location.getStAddress()).isNotNull();
    }

}
