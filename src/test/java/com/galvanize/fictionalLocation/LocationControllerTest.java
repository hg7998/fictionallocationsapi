package com.galvanize.fictionalLocation;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LocationController.class)
public class LocationControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    LocationService locationService;

    ObjectMapper mapper = new ObjectMapper();

    @Test
        //test to get all locations
    void getLocation_noParms_exists_returnsAllLocations() throws Exception {
        List<Location> locationsList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Location location = new Location(String.format("12%d N Main St", i), "Bloomington", "IL", "61701");
            locationsList.add(location);
        }

        when(locationService.getAllLocations()).thenReturn(locationsList);
        mockMvc.perform(get("/api/fictionalLocation"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)));
    }

    @Test
        //test to get a location that doesn't exist should return 204 no content
    void getLocation_noParms_noLocationsExist_returns204() throws Exception {
        List<Location> locationsList = new ArrayList<>();
        when(locationService.getAllLocations()).thenReturn(locationsList);
        mockMvc.perform(get("/api/fictionalLocation"))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
        //test to get a single address that exists, should give status 200
    void getLocation_noParms_returns200WhenValidAddressPassedIn() throws Exception {
        List<Location> locationsList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Location location = new Location(String.format("12%d N Main St", i), "Bloomington", "IL", "61701");
            locationsList.add(location);
        }
        String targetStAddress = locationsList.get(4).getStAddress();
        String targetCity = locationsList.get(4).getCity();
        when(locationService.getLocation(anyString(), anyString())).thenReturn(locationsList.get(4));
        mockMvc.perform(get(String.format("/api/fictionalLocation/%s/%s", targetStAddress, targetCity)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void addLocation_returnsStatusOk() throws Exception {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");
        when(locationService.addLocation(any(Location.class))).thenReturn(location);
        mockMvc.perform(post("/api/fictionalLocation").contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(location)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void deleteLocation_returnsNoContent() throws Exception {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");
        String targetStAddress = location.getStAddress();
        String targetCity = location.getCity();
        mockMvc.perform(delete(String.format("/api/fictionalLocation/%s/%s", targetStAddress, targetCity)))
                .andDo(print())
                .andExpect(status().isNoContent());
        verify(locationService).deleteLocation(anyString(), anyString());
    }

    @Test
    void patchLocation_returnsStatusOkAndPatchedLocation() throws Exception {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");
        String targetStAddress = location.getStAddress();
        String targetCity = location.getCity();
        when(locationService.updateLocationAddChar(anyString(),anyString(),anyString())).thenReturn(location);
        mockMvc.perform(patch(String.format("/api/fictionalLocation/%s/%s", targetStAddress, targetCity))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"charName\":\"Wario\"}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("charName").value("Wario"));
    }

    @Test
    void getLocation_returnsStatusOkAndCharacterIdentifierIfPresent() throws Exception {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");
        when(locationService.updateLocationAddChar(anyString(),anyString(),anyString())).thenReturn(location);
        mockMvc.perform(patch("/api/fictionalLocation/130 N Main St/Bloomington")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"charName\":\"Wario\"}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("charName").value("Wario"));
        when(locationService.getLocation(anyString(), anyString())).thenReturn(location);
        mockMvc.perform(get("/api/fictionalLocation/130 N Main St/Bloomington"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("charName").value("Wario"));
    }

    @Test
    void patchLocation_returnsStatusOkAndRemovesCharacter() throws Exception {
        Location location = new Location("130 N Main St", "Bloomington", "IL", "61701");
        String targetStAddress = location.getStAddress();
        String targetCity = location.getCity();
        when(locationService.updateLocationAddChar(anyString(),anyString(), anyString())).thenReturn(location);
        mockMvc.perform(patch(String.format("/api/fictionalLocation/%s/%s", targetStAddress, targetCity))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"charName\":\"Luigi\"}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("charName").value("Luigi"));
        when(locationService.updateLocationRemoveChar(anyString(),anyString())).thenReturn(location);
        mockMvc.perform(patch(String.format("/api/fictionalLocation/%s/%s/delete", targetStAddress, targetCity)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("charName").doesNotExist());
    }
}
