package com.galvanize.fictionalLocation;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

@Entity
@Table(name = "locations")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column (name = "street_address")
    private String stAddress;
    @Column (name = "city")
    private String city;
    @Column (name = "st")
    private String state;
    @Column (name = "zip")
    private String zip;
    @Column (name = "character")
    private String charName;

    public Location() {}

    public Location(String stAddress, String city, String state, String zip) {
        this.stAddress = stAddress;
        this.city = city;
        this.state = state;
        this.zip = zip;
    }

    public String getStAddress() {
        return stAddress;
    }

    public void setStAddress(String stAddress) {
        this.stAddress = stAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCharName() {
        return charName;
    }

    public void setCharName(String charName) {
        this.charName = charName;
    }

    @Override
    public String toString() {
        return String.format("Street Address: %s, City: %s, State: %s, " +
                "Zip: %s, Character: %s", this.stAddress, this.city, this.state,
                this.zip, this.charName);
    }

}
