package com.galvanize.fictionalLocation;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LocationController {

    LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/api/fictionalLocation")
    public ResponseEntity<List<Location>> getAllLocationsController() {
        List<Location> locations;
        locations = locationService.getAllLocations();
        if (locations.size() > 0) return ResponseEntity.ok(locations);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/api/fictionalLocation/{address}/{city}")
    public ResponseEntity<Location> getLocationController(@PathVariable("address") String address,
                                          @PathVariable("city") String city) {
        Location location = locationService.getLocation(address, city);
        if (location != null) return ResponseEntity.ok(location);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/api/fictionalLocation")
    public ResponseEntity<Location> postLocationController(@RequestBody Location location) {
        return ResponseEntity.ok(locationService.addLocation(location));
    }

    @DeleteMapping("/api/fictionalLocation/{address}/{city}")
    public ResponseEntity<Location> deleteLocation(@PathVariable String address,
                                         @PathVariable String city) {
        locationService.deleteLocation(address, city);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/api/fictionalLocation/{address}/{city}")
    public Location patchLocationAddChar(@PathVariable String address,
                                         @PathVariable String city,
                                         @RequestBody FictionalCharacter fictionalCharacter) {
        Location location = locationService.updateLocationAddChar(address, city, fictionalCharacter.getCharName());
        location.setCharName(fictionalCharacter.getCharName());
        return location;
    }

    @PatchMapping("/api/fictionalLocation/{address}/{city}/delete")
    public ResponseEntity<Location> patchLocationRemoveChar(@PathVariable String address,
                                                            @PathVariable String city) {
        Location location = locationService.updateLocationRemoveChar(address, city);
        location.setCharName(null);
        return ResponseEntity.ok(location);
    }


    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void invalidLocationHandler(InvalidLocationException ile) {
    }

}
