package com.galvanize.fictionalLocation;

public class FictionalCharacter {

    private String charName;

    public String getCharName() {
        return charName;
    }

    public void setCharName(String charName) {
        this.charName = charName;
    }
}
