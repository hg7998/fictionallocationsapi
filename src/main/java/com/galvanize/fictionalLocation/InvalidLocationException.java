package com.galvanize.fictionalLocation;

public class InvalidLocationException extends RuntimeException{
    public InvalidLocationException(String errorMessage) {
        super(errorMessage);
    }
}
