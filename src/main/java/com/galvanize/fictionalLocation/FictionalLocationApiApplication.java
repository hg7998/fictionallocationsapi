package com.galvanize.fictionalLocation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FictionalLocationApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FictionalLocationApiApplication.class, args);
	}

}
