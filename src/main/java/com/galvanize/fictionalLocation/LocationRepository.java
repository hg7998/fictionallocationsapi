package com.galvanize.fictionalLocation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
    Optional<Location> findByStAddressAndCity(String stAddress, String city);
    Optional<Location> findByCharName(String charName);
}
