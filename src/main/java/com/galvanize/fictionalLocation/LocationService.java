package com.galvanize.fictionalLocation;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LocationService {

    LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository)
    {this.locationRepository = locationRepository;}

    public List<Location> getAllLocations() {
        return locationRepository.findAll();
    }

    public Location getLocation(String address, String city) {
        if (locationRepository.findByStAddressAndCity(address, city).isPresent()) {
            return locationRepository.findByStAddressAndCity(address, city).get();
        }
        return null;
    }

    public Location getLocation(String charName) {
        if (locationRepository.findByCharName(charName).isPresent()) {
            return locationRepository.findByCharName(charName).get();
        }
        return null;
    }

    public Location addLocation(Location location) {
        if (locationRepository.findByStAddressAndCity(location.getStAddress(), location.getCity()).isPresent()) {
            return location;
        }
        return locationRepository.save(location);
    }

    public void deleteLocation(String address, String city) {
        Optional<Location> location = locationRepository.findByStAddressAndCity(address, city);
        if (location.isPresent()) {
            locationRepository.delete(location.get());
        }
        else {
            throw new InvalidLocationException("Location not found.");
        }
    }

    //if the character is present at a different address, it will be removed from the that
    //address and added to the new address.
    public Location updateLocationAddChar(String address, String city, String charName) {
        Optional<Location> location = locationRepository.findByStAddressAndCity(address, city);
        Optional<Location> locationToUpdate = locationRepository.findByCharName(charName);

        if (locationToUpdate.isPresent()) {
            locationToUpdate.get().setCharName(null);
            locationRepository.save(locationToUpdate.get());
        }
        if (location.isPresent()) {
            location.get().setCharName(charName);
        }
        else {
            throw new InvalidLocationException("Location not found.");
        }
        return locationRepository.save(location.get());
    }

    public Location updateLocationRemoveChar(String address, String city) {
        Optional<Location> location = locationRepository.findByStAddressAndCity(address, city);
        if (location.isPresent()) {
            location.get().setCharName(null);
            return locationRepository.save(location.get());
        }
        throw new InvalidLocationException("Location not found.");
    }
}
